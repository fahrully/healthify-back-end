'use strict';

module.exports = {
  '/gifts': {
    post: {
      controller: 'GiftController',
      method: 'createGift'
    },
    get: {
      controller: 'GiftController',
      method: 'getGifts'
    }
  },
  '/gifts/:id': {
    get: {
      controller: 'GiftController',
      method: 'getGiftById'
    },
    put: {
      controller: 'GiftController',
      method: 'patchGift'
    },
    patch: {
      controller: 'GiftController',
      method: 'updateGift'
    },
    delete: {
      controller: 'GiftController',
      method: 'deleteGift'
    }
  },
  '/gifts/:id/redeem': {
    post: {
      controller: 'GiftController',
      method: 'redeemGift'
    }
  },
  '/gifts/:id/rating': {
    post: {
      controller: 'GiftController',
      method: 'ratingGift'
    }
  },
  '/signup': {
    post: {
      controller: 'AuthController',
      method: 'signup'
    }
  },
  '/signin': {
    post: {
      controller: 'AuthController',
      method: 'signin'
    }
  },
  '/users/:id': {
    get: {
      controller: 'UserController',
      method: 'getUserById'
    },
    put: {
      controller: 'UserController',
      method: 'updateUser'
    },
    delete: {
      controller: 'UserController',
      method: 'deleteUser'
    }
  }
};
