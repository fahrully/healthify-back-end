'use strict';

/**
 * This controller exposes database REST action.
 */

const AuthService = require('../services/AuthService');

/**
 * Create user
 * @param req the request
 * @param res the response
 */
function* signup(req, res, next) {
  try {
    const user = yield AuthService.signup(req.body);
    res.status(201).json(user);
  } catch (ex) {
    next(ex);
  }
}

/**
 * Update user by id
 * @param req the request
 * @param res the response
 */

function* signin(req, res, next) {
  try {
    const users = yield AuthService.signin(req.query, req.body.username);
    res.status(200).json(users);
  } catch (ex) {
    next(ex);
  }
}

module.exports = {
  signup,
  signin
};
