'use strict';

const UserService = require('../services/UserService');

/**
 * Create user
 * @param req the request
 * @param res the response
 */
function* createUser(req, res, next) {
  try {
    const user = yield UserService.createUser(req.body);
    res.status(200).json(user);
  } catch (ex) {
    next(ex);
  }
}

/**
 * Update user by id
 * @param req the request
 * @param res the response
 */

function* updateUser(req, res, next) {
  try {
    const users = yield UserService.updateUser(req.body, req.params.id);
    res.status(200).json(users);
  } catch (ex) {
    next(ex);
  }
}

/* delete user by id
 * @param req the request
 * @param res the response
 */

function* deleteUser(req, res, next) {
  try {
    const users = yield UserService.deleteUser(req.params.id);
    res.status(200).json(users);
  } catch (ex) {
    next(ex);
  }
}

/**
 * get user by id
 * @param req the request
 * @param res the response
 */
function* getUserById(req, res, next) {
  try {
    const users = yield UserService.findUserById(req.params.id);
    res.status(200).json(users);
  } catch (ex) {
    next(ex);
  }
}

/**
 * get all user
 * @param req the request
 * @param res the response
 */
function* getUsers(req, res, next) {
  try {
    const users = yield UserService.findUsers(req.query);
    res.status(200).json(users);
  } catch (ex) {
    next(ex);
  }
}

module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser
};
