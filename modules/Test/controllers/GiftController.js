'use strict';

/**
 * This controller exposes database REST action.
 */
const GiftService = require('../services/GiftService');

/**
 * Create gift
 * @param req the request
 * @param res the response
 */
function* createGift(req, res, next) {
  try {
    const gift = yield GiftService.createGift(req.body);
    res.status(200).json(gift);
  } catch (ex) {
    next(ex);
  }
}

/**
 * Update gift by id
 * @param req the request
 * @param res the response
 */

function* updateGift(req, res, next) {
  try {
    console.log(req.body);
    const gifts = yield GiftService.updateGift(req.body, req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}
function* patchGift(req, res, next) {
  try {
    const gifts = yield GiftService.patchGift(req.body, req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

/* delete gift by id
 * @param req the request
 * @param res the response
 */

function* redeemGift(req, res, next) {
  try {
    console.log('REDEEM', req.body);
    const gifts = yield GiftService.redeemGift(req.body.redeem, req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

function* ratingGift(req, res, next) {
  try {
    const gifts = yield GiftService.ratingGift(req.body.rating, req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

function* deleteGift(req, res, next) {
  try {
    const gifts = yield GiftService.deleteGift(req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

/**
 * get gift by id
 * @param req the request
 * @param res the response
 */
function* getGiftById(req, res, next) {
  try {
    const gifts = yield GiftService.findGiftById(req.params.id);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

/**
 * get all gift
 * @param req the request
 * @param res the response
 */
function* getGifts(req, res, next) {
  try {
    const gifts = yield GiftService.findGifts(req.query);
    res.status(200).json(gifts);
  } catch (ex) {
    next(ex);
  }
}

module.exports = {
  createGift,
  redeemGift,
  patchGift,
  ratingGift,
  getGifts,
  getGiftById,
  updateGift,
  deleteGift
};
