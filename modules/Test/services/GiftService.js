'use strict';

/**
 * This service will provide database operation.
 */
const Joi = require('joi');
const logger = require('../../../common/logger');
const { Gift } = require('../../../models');

/**
 * Create new Gift
 * @param {Object} gift the new gift
 */
function* createGift(gift) {
  const giftObj = yield Gift.create(gift);
  return giftObj;
}

createGift.schema = {
  gift: Joi.object({
    product_name: Joi.string().required(),
    rating: Joi.number().min(1).max(5).required(),
    price: Joi.string().required(),
    isStock: Joi.string().required(),
    description: Joi.string().required(),
    stock: Joi.string().required(),
    redeem: Joi.boolean()
  })
};
/**
 *
 * @param {*} redeem
 * @param {*} id
 * @returns
 */
function* redeemGift(redeem, id) {
  const giftObj = yield Gift.update({ redeem }, { where: { id } });
  return giftObj;
}

function* ratingGift(rating, id) {
  const ratings = Math.round(rating);
  const giftObj = yield Gift.update({ rating: ratings }, { where: { id } });
  return giftObj;
}

/**
 * Get all Gifts
 * @param {Object} giftQuery
 */
function* findGifts(giftQuery) {
  const gift = yield Gift.findAll({
    order: [['createdAt', 'DESC']],
    limit: parseInt(giftQuery.limit),
    offset: parseInt(giftQuery.page)
  });

  return gift;
}

/**
 * Get By Gift Id
 * @param {Object} id
 */
function* findGiftById(id) {
  const gift = yield Gift.findOne({ where: { id } });
  return gift;
}

/**
 * Update Gift
 * @param {*} gift
 * @param {*} id
 */
function* updateGift(gift, id) {
  const giftObj = yield Gift.update(gift, { where: { id } });
  return giftObj;
}

updateGift.schema = {
  gift: Joi.object({
    product_name: Joi.string().required(),
    rating: Joi.number().required(),
    price: Joi.string().required(),
    isStock: Joi.string().required(),
    description: Joi.string().required(),
    stock: Joi.string().required(),
    redeem: Joi.boolean().required()
  }),
  id: Joi.string().uuid()
};

function* patchGift(gift, id) {
  const giftObj = yield Gift.update(gift, { where: { id } });
  return giftObj;
}

patchGift.schema = {
  gift: Joi.object({
    product_name: Joi.string(),
    rating: Joi.number(),
    price: Joi.string(),
    isStock: Joi.string(),
    description: Joi.string(),
    stock: Joi.string(),
    redeem: Joi.boolean()
  }),
  id: Joi.string().uuid()
};

/**
 * Update Gift by id
 * @param {*} gift
 * @param {*} id
 */

function* deleteGift(id) {
  const giftObj = yield Gift.destroy({ where: { id } });
  return giftObj;
}

module.exports = {
  createGift,
  redeemGift,
  ratingGift,
  patchGift,
  findGifts,
  findGiftById,
  updateGift,
  deleteGift
};

logger.buildService(module.exports);
