'use strict';

/**
 * This service will provide database operation.
 */
const Joi = require('joi');
const logger = require('../../../common/logger');
const { User } = require('../../../models');

/**
 * Create new User
 * @param {Object} user the new user
 */
function* createUser(user) {
  const userObj = yield User.create(user);
  return userObj;
}

createUser.schema = {
  user: Joi.object({
    email: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  })
};

/**
 * Get all Users
 * @param {Object} userQuery
 */
function* findUsers(userQuery) {
  const user = yield User.findAll(userQuery);
  return user;
}

findUsers.schema = {
  userQuery: Joi.object({
    email: Joi.string(),
    username: Joi.string(),
    password: Joi.string()
  })
};

/**
 * Get By User Id
 * @param {Object} id
 */
function* findUserById(id) {
  const user = yield User.findOne({ where: { id } });
  return user;
}

/**
 * Update User
 * @param {*} user
 * @param {*} id
 */
function* updateUser(user, id) {
  const userObj = yield User.update(user, { where: { id } });
  return userObj;
}

updateUser.schema = {
  user: Joi.object({
    email: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  }),
  id: Joi.string().uuid()
};

/**
 * Update User by id
 * @param {*} user
 * @param {*} id
 */
function* deleteUser(id) {
  const userObj = yield User.destroy({ where: { id } });
  return userObj;
}

module.exports = {
  createUser,
  findUsers,
  findUserById,
  updateUser,
  deleteUser
};

logger.buildService(module.exports);
