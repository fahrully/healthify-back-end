const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('../../../config/auth.config');

const logger = require('../../../common/logger');
const { User } = require('../../../models');

/**
 * Create new User
 * @param {Object} user the new user
 */
function* signup(user) {
  const userObj = yield User.create(user);
  return userObj;
}

signup.schema = {
  user: Joi.object({
    email: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  })
};

function* signin(user, username) {
  const userObj = yield User.findOne({ where: { username } });
  const token = jwt.sign({ id: user.id }, config.secret, {
    expiresIn: 86400 // 24 hours
  });
  const result = {
    id: userObj.id,
    username: userObj.username,
    token
  };
  return result;
}

module.exports = {
  signup,
  signin
};

logger.buildService(module.exports);
