'use strict';

module.exports = (sequelize, DataTypes) => {
  const Gift = sequelize.define('Gift', {
    product_name: {
      type: DataTypes.STRING,
      unique: false
    },
    rating: {
      type: DataTypes.FLOAT
    },
    price: {
      type: DataTypes.STRING
    },
    isStock: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.STRING
    },
    stock: {
      type: DataTypes.STRING
    },
    redeem: {
      type: DataTypes.BOOLEAN
    },
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true
    }
  });
  return Gift;
};
