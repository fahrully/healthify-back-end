-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: backend-api
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gifts` (
  `product_name` varchar(255) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `isStock` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `redeem` tinyint(1) DEFAULT NULL,
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
INSERT INTO `gifts` VALUES ('Samsung Galaxy S10 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'9f024980-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:26','2022-03-09 06:53:26'),('Samsung Galaxy S20 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'ab1a4ba0-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:47','2022-03-09 06:53:47'),('Samsung Galaxy S30 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'ad989210-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:51','2022-03-09 06:53:51'),('Samsung Galaxy S40 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'afe58f50-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:55','2022-03-09 06:53:55'),('Samsung Galaxy S11 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'b2acc5a0-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:59','2022-03-09 06:54:48'),('Samsung Galaxy S10 ',2.7,'200000','true','Ukuran Layar 62 inci. Dual Edge Super AMOLED 2960 x 1440 (QUAD HD+ 529 ppi 185:g Memori: RAM 6 GB (LPODR4\', ROM 6\' GB. MicroSD up to \'OOG3 Sistem operasi: Android B.O \'Oreo)','10',1,'e7c4d7f0-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:55:29','2022-03-09 06:55:29');
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tests` (
  `handle` varchar(255) DEFAULT NULL,
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('arul@gmail.com','arul','1234','9c1032a0-9f75-11ec-b847-f5acaef890e8','2022-03-09 06:53:21','2022-03-09 06:53:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-09 13:58:16
