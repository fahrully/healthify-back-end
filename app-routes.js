'use strict';

/**
 * This configuration of passport for express App.
 */
const _ = require('lodash');
const glob = require('glob');
const Path = require('path');
const config = require('config');
const { authJwt } = require('./middleware');
const helper = require('./common/helper');
/**
 * Configure routes for express
 * @param app the express app
 */
module.exports = (app) => {
  // load all routes.js in modules folder.
  glob
    .sync(Path.join(__dirname, './modules/**/routes.js'))
    .forEach((routes) => {
      /* Load all routes */
      _.each(
        require(Path.resolve(routes)), // eslint-disable-line import/no-dynamic-require
        (verbs, path) => {
          _.each(verbs, (def, verb) => {
            const controllerPath = Path.join(
              Path.dirname(routes),
              `./controllers/${def.controller}`
            );
            const method = require(controllerPath)[def.method]; // eslint-disable-line import/no-dynamic-require
            console.log(method, routes, verb, path);
            if (!method) {
              throw new Error(`${def.method} is undefined`);
            }
            const actions = [];
            actions.push((req, res, next) => {
              req.signature = `${def.controller}#${def.method}`;
              next();
            });
            actions.push(method);
            console.log(path);
            if (path === '/signin' || path === '/signup') {
              app[verb](
                `/api/${config.version}${path}`,
                helper.autoWrapExpress(actions)
              );
            } else {
              app[verb](
                `/api/${config.version}${path}`,
                [authJwt.verifyToken],
                helper.autoWrapExpress(actions)
              );
            }
          });
        }
      );
    });
};
