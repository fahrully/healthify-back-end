## Available Scripts

In the project directory, you can run:

### `npm install`

install the dependencies packages.

### `node app.js`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Runs automate testing using Postman Newman

### `newman run "https://www.getpostman.com/collections/d02271d59559571d7cf4"`
